#include "virtual_machine.h"

#include "instructions/print.h"
#include "instructions/add.h"
#include "instructions/sub.h"
#include "instructions/mult.h"
#include "instructions/div.h"
#include "instructions/jmp.h"
#include "instructions/jmpz.h"
#include "instructions/jmpnz.h"
#include "instructions/push.h"
#include "instructions/push_off.h"
#include "instructions/pop.h"
#include "instructions/halt.h"
#include "instructions/fpstore.h"
#include "instructions/fpload.h"
#include "instructions/jmpgz.h"
#include "instructions/jmpgez.h"
#include "instructions/jmplz.h"
#include "instructions/jmplez.h"
#include "instructions/call.h"
#include "instructions/return.h"
#include "instructions/return_val.h"

#include <fstream>
#include <string>
#include <sstream>

VirtualMachine::VirtualMachine()
{
    this->frame_pointer = 0;
    this->stack_pointer = 0;
    this->instruction_register = 0;
    this->instruction_pointer = 0;
    this->cur_instruction = NULL;
    this->is_halted = false;
    this->stack_size = 1024;
    this->stack = new vm_type[this->stack_size];
}

VirtualMachine::~VirtualMachine()
{
    for (unsigned int i = 0; i < this->code.size(); i++)
        delete this->code[i];
    code.clear();
}

bool VirtualMachine::load_program(std::string path)
{
    std::ifstream file_stream(path, std::ios_base::in);
    std::string tmp;

    if (!file_stream.is_open())
    {
        std::cerr << "errore caricamento programma: " << path << std::endl;
        return false;
    }

    while (!file_stream.eof())
    {
        std::getline(file_stream, tmp);
        this->decode_instruction(tmp);
    }

    return true;
}

void VirtualMachine::decode_instruction(std::string& ins)
{
    /* FIXME: SE SI POSIZIONE L'IF DI jmp PRIMA DI QUELLO DI jmpz
     * ALLORA QUEST'ULTIMO NON VERRA' MAI RAGGIUNTO PERCHE' LA
     * STRINGA jmp E' SEMPRE CONTENUTA IN QUELLA jmpz
     */

    if (ins.find("jmpnz", 0) != std::string::npos)
        this->code.push_back(new Jmpnz<int>(this, ins));

    else if (ins.find("jmpz", 0) != std::string::npos)
        this->code.push_back(new Jmpz<int>(this, ins));

    else if (ins.find("call", 0) != std::string::npos)
        this->code.push_back(new Call<int>(this, ins));

    else if (ins.find("retval", 0) != std::string::npos)
        this->code.push_back(new ReturnVal(this));

    else if (ins.find("ret", 0) != std::string::npos)
        this->code.push_back(new Return(this));

    else if (ins.find("jmpgez", 0) != std::string::npos)
        this->code.push_back(new Jmpgez<int>(this, ins));

    else if (ins.find("jmpgz", 0) != std::string::npos)
        this->code.push_back(new Jmpgz<int>(this, ins));

    else if (ins.find("jmplez", 0) != std::string::npos)
        this->code.push_back(new Jmplez<int>(this, ins));

    else if (ins.find("jmplz", 0) != std::string::npos)
        this->code.push_back(new Jmplz<int>(this, ins));

    else if (ins.find("jmp", 0) != std::string::npos)
        this->code.push_back(new Jmp<int>(this, ins));

    else if (ins.find("fpstore", 0) != std::string::npos)
        this->code.push_back(new Fpstore<VirtualMachine::vm_type>(this, ins));

    else if (ins.find("fpload", 0) != std::string::npos)
        this->code.push_back(new Fpload<int>(this, ins));

    else if (ins.find("add", 0) != std::string::npos)
        this->code.push_back(new Add(this));

    else if (ins.find("sub", 0) != std::string::npos)
        this->code.push_back(new Sub(this));

    else if (ins.find("mult", 0) != std::string::npos)
        this->code.push_back(new Mult(this));

    else if (ins.find("div", 0) != std::string::npos)
        this->code.push_back(new Div(this));

    else if (ins.find("pop", 0) != std::string::npos)
        this->code.push_back(new Pop(this));

    else if (ins.find("print", 0) != std::string::npos)
        this->code.push_back(new Print(this));

    else if (ins.find("pushoff", 0) != std::string::npos)
        this->code.push_back(new PushOff<VirtualMachine::vm_type>(this, ins));

    else if (ins.find("push", 0) != std::string::npos)
        this->code.push_back(new Push<VirtualMachine::vm_type>(this, ins));

    else if (ins.find("halt", 0) != std::string::npos)
        this->code.push_back(new Halt(this));

    else
    {
        std::cerr << "->" << ins[0] << "<-" << std::endl;
        std::cerr << ins << " istruzione non riconosciuta" << std::endl;
        //		exit(1);
        // TODO: LANCIA ECCEZIONE PER ISTRUZIONE NON RICONOSCIUTA
    }
}

void VirtualMachine::stack_dump(std::ostream& out_stream) const
{
    out_stream << "STACK DUMP" << std::endl;
    for (int i = this->stack_size - 1; i >= 0; i--)
        out_stream << i << " " << stack[i] << std::endl;

    out_stream << "stack_pointer: " << this->stack_pointer
        << std::endl;
    out_stream << "frame_pointer: " << this->frame_pointer
        << std::endl;
    out_stream << std::endl;
}

void VirtualMachine::code_dump(std::ostream& out_stream) const
{
    out_stream << "CODE DUMP" << std::endl;
    for (int i = this->code.size() - 1; i >= 0; i--)
        out_stream << i << " " << code[i]->to_string() << std::endl;
    out_stream << std::endl;
}


void VirtualMachine::fetch()
{
    this->cur_instruction =
        this->code[this->instruction_register];
}

void VirtualMachine::decode()
{
    this->cur_instruction->execute();
    this->instruction_register = this->instruction_pointer;
}

void VirtualMachine::start()
{
    while (!this->is_halted)
    {
        this->fetch();
        this->decode();
    }
}


