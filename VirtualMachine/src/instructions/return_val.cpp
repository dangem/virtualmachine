#include "return_val.h"

ReturnVal::ReturnVal(VirtualMachine* virtual_machine)
    : Instruction(virtual_machine)
{
    this->str_instruction = "retval";
}

void ReturnVal::execute() const
{
    this->virtual_machine->instruction_pointer
        = this->virtual_machine->stack[this->virtual_machine->frame_pointer - 2];

    this->virtual_machine->stack[this->virtual_machine->frame_pointer - 2]
        = this->virtual_machine->stack[this->virtual_machine->stack_pointer - 1];

    this->virtual_machine->stack_pointer = this->virtual_machine->frame_pointer - 1;

    this->virtual_machine->frame_pointer
        = this->virtual_machine->stack[this->virtual_machine->frame_pointer - 1];
}

