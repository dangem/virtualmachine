#ifndef CALL_H_
#define CALL_H_

#include "two_operand_instr.h"

template <typename T>
class Call : public TwoOperandInstr<T>
{
    public:
        Call(VirtualMachine* vm, std::string& instruction);
        virtual ~Call() = default;

        virtual void execute() const;
};

template <typename T>
Call<T>::Call(VirtualMachine* virtual_machine, std::string& instruction)
    : TwoOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Call<T>::execute() const
{
    this->virtual_machine->stack_pointer += this->operand_1 + 2;

    this->virtual_machine->
        stack[this->virtual_machine->stack_pointer - this->operand_1 - 1] =
            this->virtual_machine->frame_pointer;

    this->virtual_machine->frame_pointer = this->virtual_machine->stack_pointer
            - this->operand_1;

    this->virtual_machine->
        stack[this->virtual_machine->stack_pointer - this->operand_1 - 2] =
            this->virtual_machine->instruction_pointer + 1;

    this->virtual_machine->instruction_pointer = this->operand_2;
}

#endif
