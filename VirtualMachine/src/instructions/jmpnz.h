#ifndef JMPNZ_H_
#define JMPNZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmpnz : public OneOperandInstr<T>
{
    public:
        Jmpnz(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Jmpnz() = default;

        virtual void execute() const;
};

template <typename T>
Jmpnz<T>::Jmpnz(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmpnz<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] != 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}


#endif
