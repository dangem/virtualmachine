#ifndef JMPGZ_H_
#define JMPGZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmpgz : public OneOperandInstr<T>
{
    public:
        Jmpgz(VirtualMachine* vm, std::string& instruction);
        virtual ~Jmpgz() = default;

        virtual void execute() const;

};

template <typename T>
Jmpgz<T>::Jmpgz(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmpgz<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] > 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}

#endif
