#include "sub.h"

Sub::Sub(VirtualMachine* virtual_machine)
    : Instruction(virtual_machine)
{
    this->str_instruction = "sub";
}

void Sub::execute() const
{
    VirtualMachine::vm_type op1 = this->virtual_machine->
        stack[this->virtual_machine->stack_pointer - 2];

    VirtualMachine::vm_type op2 = this->virtual_machine->
        stack[this->virtual_machine->stack_pointer - 1];

    this->virtual_machine->stack[this->virtual_machine->stack_pointer - 2] =
        op1 - op2;

    this->virtual_machine->stack_pointer -= 1;
    this->virtual_machine->instruction_pointer++;
}
