#ifndef RETURN_VAL_H_
#define RETURN_VAL_H_

#include "instruction.h"

class ReturnVal : public Instruction
{
    public:
        ReturnVal(VirtualMachine* vm);
        virtual ~ReturnVal() = default;

        virtual void execute() const;
};

#endif
