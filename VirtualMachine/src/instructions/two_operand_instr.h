#ifndef TWO_OPERAND_INSTR_H_
#define TWO_OPERAND_INSTR_H_

#include "instruction.h"

#include <boost/algorithm/string.hpp>
#include <string>
#include <sstream>

template <typename T>
class TwoOperandInstr : public Instruction
{
    public:
        TwoOperandInstr(VirtualMachine* virtual_machine
                , std::string& instruction);
        virtual ~TwoOperandInstr() = default;


    protected:
        std::string str_operand_1;
        std::string str_operand_2;
        // IL PRIMO OPERANDO E' SEMPRE L'OFFSET PER fp
        int operand_1;
        T operand_2;
};


template <typename T>
TwoOperandInstr<T>::TwoOperandInstr(VirtualMachine* virtual_machine
        , std::string& instruction)
        : Instruction(virtual_machine)
{
    this->operand_1 = 0;
    this->str_instruction = instruction.c_str();

    std::vector<std::string> splitted;
    boost::split(splitted, instruction, boost::is_any_of(" "));
    this->str_operand_1 = splitted[1];
    this->str_operand_2 = splitted[2];

    std::stringstream ss(this->str_operand_1);
    ss >> this->operand_1;

    std::stringstream ss1(this->str_operand_2);
    ss1 >> this->operand_2;
}

#endif
