#ifndef ADD_H_
#define ADD_H_

#include "instruction.h"

class Add : public Instruction
{
    public:
        Add(VirtualMachine* virtual_machine);
        virtual ~Add() = default;

        void execute() const;

};


#endif
