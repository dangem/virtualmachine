#ifndef SUB_H_
#define SUB_H_

#include "instruction.h"

class Sub : public Instruction
{
    public:
        Sub(VirtualMachine* virtual_machine);
        virtual ~Sub() = default;

        void execute() const;
};


#endif
