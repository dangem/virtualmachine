#ifndef FPLOAD_H_
#define FPLOAD_H_

#include "one_operand_instr.h"

template <typename T>
class Fpload : public OneOperandInstr<T>
{
    public:
        Fpload(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Fpload() = default;

        void execute() const;
};

template <typename T>
Fpload<T>::Fpload(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Fpload<T>::execute() const
{
    this->virtual_machine->
    stack[this->virtual_machine->stack_pointer] =
        this->virtual_machine->
        stack[this->virtual_machine->frame_pointer + this->operand];

    this->virtual_machine->stack_pointer++;
    this->virtual_machine->instruction_pointer++;
}

#endif
