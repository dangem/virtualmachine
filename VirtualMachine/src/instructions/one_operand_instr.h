#ifndef ONE_OPERAND_INSTR_H_
#define ONE_OPERAND_INSTR_H_

#include "instruction.h"

#include <boost/algorithm/string.hpp>
#include <string>
#include <sstream>

template <typename T>
class OneOperandInstr : public Instruction
{
    public:
        OneOperandInstr(VirtualMachine* virtual_machine
                , std::string& instruction);
        virtual ~OneOperandInstr() = default;

        void set_operand(T operand);


    protected:
        std::string str_operand;
        T operand;
};


template <typename T>
OneOperandInstr<T>::OneOperandInstr(VirtualMachine* virtual_machine
        , std::string& instruction)
        : Instruction(virtual_machine)
{
    this->str_instruction = instruction.c_str();

    std::vector<std::string> splitted;
    boost::split(splitted, instruction, boost::is_any_of(" "));
    this->str_operand = splitted[1];

    std::stringstream ss(this->str_operand);
    ss >> this->operand;
}

template <typename T>
void OneOperandInstr<T>::set_operand(T operand)
{
    this->operand = operand;
}


#endif
