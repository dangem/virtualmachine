#ifndef JMPLEZ_H_
#define JMPLEZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmplez : public OneOperandInstr<T>
{
    public:
        Jmplez(VirtualMachine* vm, std::string& instruction);
        virtual ~Jmplez() = default;

        virtual void execute() const;

};

template <typename T>
Jmplez<T>::Jmplez(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmplez<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] <= 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}

#endif
