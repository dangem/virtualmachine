#ifndef JMPGEZ_H_
#define JMPGEZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmpgez : public OneOperandInstr<T>
{
    public:
        Jmpgez(VirtualMachine* vm, std::string& instruction);
        virtual ~Jmpgez() = default;

        virtual void execute() const;

};

template <typename T>
Jmpgez<T>::Jmpgez(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmpgez<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] >= 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}


#endif
