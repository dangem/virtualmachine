#ifndef RETURN_H_
#define RETURN_H_

#include "instruction.h"

class Return : public Instruction
{
    public:
        Return(VirtualMachine* vm);
        virtual ~Return() = default;

        virtual void execute() const;
};

#endif
