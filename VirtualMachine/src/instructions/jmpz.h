#ifndef JMPZ_H_
#define JMPZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmpz : public OneOperandInstr<T>
{
    public:
        Jmpz(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Jmpz() = default;

        virtual void execute() const;
};

template <typename T>
Jmpz<T>::Jmpz(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmpz<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] == 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}

#endif
