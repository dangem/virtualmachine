#ifndef PUSH_H_
#define PUSH_H_

#include "one_operand_instr.h"

template <typename T>
class Push : public OneOperandInstr<T>
{
    public:
        Push(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Push() = default;

        void execute() const;
};

template <typename T>
Push<T>::Push(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Push<T>::execute() const
{
    this->virtual_machine->
        stack[this->virtual_machine->stack_pointer] = this->operand;
    this->virtual_machine->stack_pointer++;
    this->virtual_machine->instruction_pointer++;
}

#endif
