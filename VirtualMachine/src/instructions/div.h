#ifndef DIV_H_
#define DIV_H_

#include "instruction.h"

class Div : public Instruction
{
    public:
        Div(VirtualMachine* virtual_machine);
        virtual ~Div() = default;

        void execute() const;
};

#endif
