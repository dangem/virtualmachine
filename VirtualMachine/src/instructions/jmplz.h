#ifndef JMPLZ_H_
#define JMPLZ_H_

#include "one_operand_instr.h"

template <typename T>
class Jmplz : public OneOperandInstr<T>
{
    public:
        Jmplz(VirtualMachine* vm, std::string& instruction);
        virtual ~Jmplz() = default;

        virtual void execute() const;

};

template <typename T>
Jmplz<T>::Jmplz(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmplz<T>::execute() const
{
    if (this->virtual_machine->
            stack[this->virtual_machine->stack_pointer - 1] < 0)
        this->virtual_machine->instruction_pointer = this->operand;
    else
        this->virtual_machine->instruction_pointer++;

}

#endif
