#ifndef POP_H_
#define POP_H_

#include "instruction.h"

class Pop : public Instruction
{
    public:
        Pop(VirtualMachine* virtual_machine);
        virtual ~Pop() = default;

        virtual void execute() const;
};


#endif
