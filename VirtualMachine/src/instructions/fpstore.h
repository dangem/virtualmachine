#ifndef FPSTORE_H_
#define FPSTORE_H_

#include "one_operand_instr.h"

template <typename T>
class Fpstore : public OneOperandInstr<T>
{
    public:
        Fpstore(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Fpstore() = default;

        void execute() const;
};

template <typename T>
Fpstore<T>::Fpstore(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Fpstore<T>::execute() const
{
    VirtualMachine::vm_type tmp =
            this->virtual_machine->stack[this->virtual_machine->stack_pointer - 1];
    // ALTRIMENTI DA' PROBLEMI PERCHE' operand E' UN PARAMETRO TEMPLATE
    int tmp_index = this->operand;
    this->virtual_machine->
        stack[this->virtual_machine->frame_pointer + tmp_index] = tmp;

    this->virtual_machine->instruction_pointer++;
}


#endif
