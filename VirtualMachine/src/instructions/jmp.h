#ifndef JMP_H_
#define JMP_H_

#include "one_operand_instr.h"

template <typename T>
class Jmp : public OneOperandInstr<T>
{
    public:
        Jmp(VirtualMachine* virtual_machine, std::string& instruction);
        virtual ~Jmp() = default;

        Jmp<T> clone() const;
        virtual void execute() const;
};


template <typename T>
Jmp<T>::Jmp(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void Jmp<T>::execute() const
{
    this->virtual_machine->instruction_pointer = this->operand;
}

template <typename T>
Jmp<T> Jmp<T>::clone() const
{
    return *this;
}

#endif
