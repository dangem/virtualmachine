#include "print.h"

#include <iostream>


Print::Print(VirtualMachine* virtual_machine)
    : Instruction(virtual_machine)
{
    this->str_instruction = "print";
}

void Print::execute() const
{
    std::cout << this->virtual_machine->stack[this->virtual_machine->stack_pointer - 1]
                  << std::endl;
    this->virtual_machine->instruction_pointer++;
}

