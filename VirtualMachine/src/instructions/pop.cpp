#include "pop.h"

Pop::Pop(VirtualMachine* virtual_machine)
    : Instruction(virtual_machine)
{
    this->str_instruction = "pop";
}

void Pop::execute() const
{
    this->virtual_machine->stack_pointer--;
    this->virtual_machine->instruction_pointer++;
}


