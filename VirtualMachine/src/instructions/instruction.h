#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_

#include "../virtual_machine.h"

class Instruction
{
    public:
        Instruction(VirtualMachine* virtual_machine);
        virtual ~Instruction();

        virtual void execute() const = 0;
        virtual std::string to_string() const;

    protected:
        VirtualMachine* virtual_machine;
        std::string str_instruction;
};


#endif
