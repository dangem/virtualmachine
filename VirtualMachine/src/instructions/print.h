#ifndef PRINT_H_
#define PRINT_H_

#include "instruction.h"

class Print : public Instruction
{
    public:
        Print(VirtualMachine* virtual_machine);
        virtual ~Print() = default;

        virtual void execute() const;
};


#endif
