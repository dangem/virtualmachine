#ifndef MULT_H_
#define MULT_H_

#include "instruction.h"

class Mult : public Instruction
{
    public:
        Mult(VirtualMachine* virtual_machine);
        virtual ~Mult() = default;

        void execute() const;
};



#endif
