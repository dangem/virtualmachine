#include "halt.h"

Halt::Halt(VirtualMachine* virtual_machine)
    : Instruction(virtual_machine)
{
    this->str_instruction = "halt";
}

void Halt::execute() const
{
    this->virtual_machine->is_halted = true;
}

