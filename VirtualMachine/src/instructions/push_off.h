#ifndef PUSH_OFF_H_
#define PUSH_OFF_H_

#include "one_operand_instr.h"
#include <iostream>

template <typename T>
class PushOff : public OneOperandInstr<T>
{
    public:
        PushOff(VirtualMachine* vm, std::string& instruction);
        virtual ~PushOff() = default;

        virtual void execute() const;
};

template <typename T>
PushOff<T>::PushOff(VirtualMachine* virtual_machine, std::string& instruction)
    : OneOperandInstr<T>(virtual_machine, instruction)
{
}

template <typename T>
void PushOff<T>::execute() const
{
    this->virtual_machine->stack[this->virtual_machine->stack_pointer + this->operand]
        = this->virtual_machine->stack[this->virtual_machine->stack_pointer - 1];
    this->virtual_machine->instruction_pointer++;
}

#endif
