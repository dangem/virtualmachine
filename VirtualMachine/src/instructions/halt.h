#ifndef HALT_H_
#define HALT_H_

#include "instruction.h"

class Halt : public Instruction
{
    public:
        Halt(VirtualMachine* virtual_machine);
        virtual ~Halt() = default;

        virtual void execute() const;
};


#endif
