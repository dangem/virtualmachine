#ifndef VIRTUAL_MACHINE_H_
#define VIRTUAL_MACHINE_H_

#include <vector>
#include <string>
#include <iosfwd>

class Instruction;

class VirtualMachine
{
        template <typename T> friend class Jmp;
        template <typename T> friend class Jmpz;
        template <typename T> friend class Jmpnz;
        template <typename T> friend class Jmpgz;
        template <typename T> friend class Jmpgez;
        template <typename T> friend class Jmplz;
        template <typename T> friend class Jmplez;
        template <typename T> friend class Push;
        template <typename T> friend class PushOff;
        template <typename T> friend class Fpstore;
        template <typename T> friend class Fpload;
        template <typename T> friend class Call;
        friend class Print;
        friend class Pop;
        friend class Add;
        friend class Sub;
        friend class Mult;
        friend class Div;
        friend class Halt;
        friend class Return;
        friend class ReturnVal;

    public:
        VirtualMachine();
        // FIXME: SERVE UN COSTRUTTORE DI COPIA
        ~VirtualMachine();

        bool load_program(std::string path);
        void start();

        void stack_dump(std::ostream& out_stream) const;
        void code_dump(std::ostream& out_stream) const;

        /* IN MODO TALE CHE SI POSSA CAMBIARE TIPO GESTITO DALLO STACK
         * SENZA AFFLIGGERE LE CLASSI Instruction
         */
        typedef int vm_type;

    private:
        // METODI PRIVATI
        void decode_instruction(std::string& ins);
        void fetch();
        void decode();

        // CAMPI PRIVATI
        size_t stack_size;
        vm_type* stack;
        std::vector<Instruction*> code;

        Instruction* cur_instruction;

        int stack_pointer;
        int frame_pointer;
        int instruction_register;
        int instruction_pointer;

        bool is_halted;
};


#endif
